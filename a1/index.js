

// PART 1

	db.fruits.aggregate([
        {
            $match: {"onSale": true}
        },
        {
            $group: {_id: "fruitOnSale", count: {$sum:1}}
        },
        {
            $project: {"_id": 0}
        }
    ]);

// PART 2

db.fruits.aggregate([
        {
            $match: {"stock": {$gte: 20}}
        },
        {
            $group: {_id: "total", count: {$sum:1}}
        },
        {
            $project: {"_id": 0}
        }
    ]);

// PART 3

	db.fruits.aggregate([
		{
			$match:{"onSale": true}
		},
		{
			$group: {_id: "$supplier_id", avgPrice: {$avg: "$price"}}
		}
	
		]);
	
// PART 4

db.fruits.aggregate([
	{
		$match:{"onSale": true}
	},
    {
        $group: {_id: "$supplier_id", maxPrice: {$max: "$price"}}
    }
    ]);

// PART 5

db.fruits.aggregate([
	{
		$match:{"onSale": true}
	},
    {
        $group: {_id: "$supplier_id", minPrice: {$min: "$price"}}
    }
    ]);  


